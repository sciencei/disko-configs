{ disk, swapsize, ... }: {
  disko.devices = {
    disk = {
      vdb = {
        type = "disk";
        device = disk;
        content = {
          type = "gpt";
          partitions = {
            # EFI boot partition, unencrypted
            ESP = {
              size = "1G";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [
                  "defaults"
                ];
              };
            };
            # encrypted luks partition, everything else lives here
            luks = {
              size = "100%";
              content = {
                type = "luks";
                name = "crypted";
                extraOpenArgs = [ "--allow-discards" ];
                # prompt for a password for the luks partition at creation time
                askPassword = true;
                # even though we're using btrfs, we have lvm on top of it so we can also have a
                # swap partition inside our encrypted partition
                content = {
                  type = "lvm_pv";
                  vg = "pool";
                };
              };
            };
          };
        };
      };
    };
    # this is actually inside our luks partition and describes everything except the boot
    lvm_vg = {
      pool = {
        type = "lvm_vg";
        lvs = {
          swap = {
            size = swapsize;
            content = {
              type = "swap";
              # the primary reason we have this is to allow hibernation
              # make sure it's at least as big as the amount of ram
              resumeDevice = true;
              # already encrypted under our main luks partition
              randomEncryption = false;
            };
          };
          # all our actual stuff
          system = {
            size = "100%FREE";
            content = {
              type = "btrfs";
              extraArgs = [ "-f" ];
              subvolumes = {
                "/root" = {
                  mountpoint = "/";
                  mountOptions = [ "compress=zstd:2" "noatime" ];
                };
                "/home" = {
                  mountpoint = "/home";
                  mountOptions = [ "compress=zstd:2" "noatime" ];
                };
                "/nix" = {
                  mountpoint = "/nix";
                  mountOptions = [ "compress=zstd:2" "noatime" ];
                };
              };
            };
          };
        };
      };
    };
  };
}
